FROM ubuntu:latest
MAINTAINER Ben Blanks <ben@d15k.com>

##
# Install the packages
#
RUN apt-get update --no-install-recommends -y && \
    apt-get install --no-install-recommends -y software-properties-common language-pack-en-base &&\
    apt-get update --no-install-recommends -y

RUN apt-get install --no-install-recommends -y \
    vim \
    apache2 \
    openssl \
    make \
    ca-certificates \
    curl \
    wget \
    git \
    less \
    openssh-client

##
# Clean the apt-get repo cache.
#
RUN apt-get clean &&\
    rm -rf /var/lib/apt/lists/*

##
# Remove the html directory so we have a clean www directory.
#
RUN rm -rf /var/www/html &&\
    rm /etc/apache2/sites-enabled/000-default.conf &&\
    rm /etc/apache2/conf-enabled/other-vhosts-access-log.conf

##
# Configure Apache
#
RUN a2enmod headers rewrite ssl

##
# Create the WWW user.
#
RUN useradd www &&\
    mkdir -p /home/www &&\
    chown www:www /home/www &&\
    chmod 0700 /home/www &&\
    chown www:www-data /var/www

COPY scripts/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN mkdir /docker-tools &&\
    chmod 0755 /usr/local/bin/docker-entrypoint &&\
    ln -s /usr/local/bin/docker-entrypoint /

VOLUME /var/www/
VOLUME /etc/apache2/sites-enabled/
VOLUME /docker-tools

EXPOSE 80 443
ENTRYPOINT ["docker-entrypoint"]
